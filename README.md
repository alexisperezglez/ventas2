Ventas aplicacion web de ejemplo

#### Pasos

##### Clonar Repo
```
$  git clone https://gitlab.com/alexisperezglez/ventas2
```

##### Compilar jar
```
$ mvn clean install 
```

##### Correr aplicacion
```
$ mvn sprintboot:run 
```

##### Comprobar aplicacion 
```
$ curl http://localhost:8080/actuator/health
```
