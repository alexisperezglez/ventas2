FROM java:8
COPY ./target/*.jar /app/ventas-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/app/ventas-0.0.1-SNAPSHOT.jar"]