package com.shoppingcar.demo.service;

import com.shoppingcar.demo.EntityNotFoundException;
import com.shoppingcar.demo.models.domain.Venta;
import com.shoppingcar.demo.models.dto.VentaDTO;
import com.shoppingcar.demo.models.mapper.VentaMapper;
import com.shoppingcar.demo.repository.VentaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service implementation for the Sale entity.
 */
@Service
@Transactional
@Slf4j
public class VentaServiceImpl implements IVentaService {

    private final VentaRepository ventaRepository;

    private VentaMapper ventaMapper;

    public VentaServiceImpl(VentaRepository ventaRepository, VentaMapper ventaMapper) {
        this.ventaRepository = ventaRepository;
        this.ventaMapper = ventaMapper;
    }

    /**
     * Guardar Venta
     *
     * @param ventaDTO
     * @return VentaDTO
     */
    public VentaDTO save(VentaDTO ventaDTO) {
        log.debug("Guardar Venta : {}", ventaDTO);
        Venta venta = ventaMapper.toEntity(ventaDTO);
        venta = ventaRepository.save(venta);
        return ventaMapper.toDto(venta);
    }


    /**
     * Lista Ventas.
     *
     * @return VentaDTO
     */
    @Transactional(readOnly = true)
    public List<VentaDTO> findAll() {
        log.debug("Lista Ventas");
        return ventaRepository.findAll().stream()
                .map(ventaMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Lista Ventas por id.
     *
     * @param id
     * @return VentaDTO[]
     */
    @Transactional(readOnly = true)
    public Optional<VentaDTO> findOne(Long id) {
        log.debug("Obtener Ventas : {}", id);
        return ventaRepository.findById(id)
                .map(ventaMapper::toDto);
    }


    @Transactional(readOnly = true)
    public List<VentaDTO> findAllSalesById(Long id) {
        log.debug("Ventas por usuario : {}", id);
        try {
            return ventaRepository.findAllByCliente_Id(id).stream()
                    .map(ventaMapper::toDto)
                    .collect(Collectors.toCollection(LinkedList::new));
        } catch (Exception e) {
            throw new EntityNotFoundException(Venta.class);
        }
    }

    /*private Single<List<Venta>> findAllSalesInRepository(Long id) {
        return Single.create(singleSubscriber -> {
            List<Venta> ventas = ventaRepository.findAllByClient_Id(id);

            if(!ObjectUtils.isEmpty(ventas)){
                singleSubscriber.onSuccess(ventas);
            }else {
                singleSubscriber.onError(new EntityNotFoundException(Venta.class));
            }
        });
    }

    private List<VentaDTO> toBookResponseList(List<Venta> bookList) {
        return bookList
                .stream()
                .map(ventaMapper:: toDto)
                .collect(Collectors.toList());
    }*/
}
