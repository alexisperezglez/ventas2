package com.shoppingcar.demo.service;

import com.shoppingcar.demo.models.dto.ClienteDTO;
import rx.Single;

import java.util.List;

/**
 * Service interface for the Client entity.
 */
public interface IClienteService {

    ClienteDTO save(ClienteDTO clienteDTO);

    List<ClienteDTO> findAll();

    Single<Object> findAllById(Long id);
}
