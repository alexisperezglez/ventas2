package com.shoppingcar.demo.service;

import com.shoppingcar.demo.models.domain.Producto;
import com.shoppingcar.demo.models.dto.ProductoDTO;
import com.shoppingcar.demo.models.mapper.ProductoMapper;
import com.shoppingcar.demo.exceptions.ProductoException;
import com.shoppingcar.demo.repository.ProductoRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service implementation for the Product entity.
 */
@Service
@Transactional
@Log4j2
public class ProductoServiceImpl implements IProductoService {

    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private ProductoMapper productoMapper;

    /**
     * Guardar Producto
     *
     * @param productoDTO
     * @return ProductoDTO
     */
    public ProductoDTO save(ProductoDTO productoDTO) throws ProductoException {
        log.debug("Guardar Producto : {}", productoDTO);

        try{
            Producto producto = productoMapper.toEntity(productoDTO);
            producto = productoRepository.save(producto);
            return productoMapper.toDto(producto);
        } catch (Exception ex) {
            throw new ProductoException(String.format("Ha ocurrido un error al intentar guardar el producto: %s", ex.getMessage()));
        }
    }

    /**
     * Producto por id
     *
     * @param id
     * @return ProductoDTO
     */
    @Transactional(readOnly = true)
    public ProductoDTO findOne(Long id) throws ProductoException {
        log.debug("[ProductoService] - findOne {} " + "productId: " + id);

        try{
            return productoMapper.toDto(this.findById(id));
        } catch (ProductoException e) {
            throw e;
        } catch (Exception exception) {
            throw new ProductoException(String.format("Ha ocurrido un error al intentar obtener el producto : %s", exception.getMessage()));
        }
    }

    /**
     * Producto por id
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Producto findById(Long id) throws ProductoException {
        log.debug("[ProductoService] - findById {} " + "productId: " + id);
        try{
            return productoRepository.findById(id)
                    .orElseThrow(() -> new ProductoException(String.format("No existe producto con el id %s", id)));
        } catch (Exception e) {
            throw e;
        } finally {
            return null;
        }
    }


    /**
     * Lista Productos
     *
     * @return ProductoDTO[]
     */
    @Transactional(readOnly = true)
    public List<ProductoDTO> findAll() throws ProductoException {
        log.debug("[ProductoService] - getAll {}");
        try{
            return productoMapper.toDto(productoRepository.findAll());
        } catch (Exception e) {
            throw new ProductoException(String.format("Ha ocurrido un error al intentar obtener la lista de productos: %s", e.getMessage()));
        }
    }

    /**
     * Eliminar Producto
     *
     * @param id
     */
    public void delete(Long id) throws ProductoException {
        log.info("[ProductoService] - deletePeripheralDeviceFromGateway {} " + "productId: " + id);

        try{
            productoRepository.deleteById(id);
        } catch (Exception e) {
            throw new ProductoException(String.format("Ha ocurrido un error al intentar eliminar el producto: %s", e.getMessage()));
        }
    }
}
