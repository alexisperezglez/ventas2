package com.shoppingcar.demo.service;

import com.shoppingcar.demo.models.dto.ProductoDTO;
import com.shoppingcar.demo.exceptions.ProductoException;

import java.util.List;

/**
 * Service interface for the Client entity.
 */
public interface IProductoService {

    ProductoDTO save(ProductoDTO productoDTO) throws ProductoException;

    List<ProductoDTO> findAll() throws ProductoException;

    ProductoDTO findOne(Long id) throws ProductoException;

    void delete(Long id) throws ProductoException;
}
