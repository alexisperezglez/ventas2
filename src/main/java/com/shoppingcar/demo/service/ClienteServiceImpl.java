package com.shoppingcar.demo.service;


import com.shoppingcar.demo.models.domain.Cliente;
import com.shoppingcar.demo.models.dto.ClienteDTO;
import com.shoppingcar.demo.models.mapper.ClienteMapper;
import com.shoppingcar.demo.repository.ClienteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rx.Single;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service implementation for the Client entity.
 */
@Service
@Transactional
@Slf4j
public class ClienteServiceImpl implements IClienteService {

    private final ClienteRepository clienteRepository;

    @Autowired
    private ClienteMapper clienteMapper;

    public ClienteServiceImpl(ClienteRepository clienteRepository, ClienteMapper clienteMapper) {
        this.clienteRepository = clienteRepository;
        this.clienteMapper = clienteMapper;
    }

    /**
     * Guardar Cliente
     *
     * @param clienteDTO
     * @return ClienteDTO
     */
    public ClienteDTO save(ClienteDTO clienteDTO) {
        log.debug("Guardar Cliente : {}", clienteDTO);
        Cliente cliente = clienteMapper.toEntity(clienteDTO);
        cliente = clienteRepository.save(cliente);
        return clienteMapper.toDto(cliente);
    }


    /**
     * Lista Clientes.
     *
     * @return ClienteDTO[]
     */
    @Transactional(readOnly = true)
    public List<ClienteDTO> findAll() {
        log.debug("Lista Clientes");
        return clienteRepository.findAll().stream()
                .map(clienteMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Single<Object> findAllById(Long id) {
        return null;
    }


}
