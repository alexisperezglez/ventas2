package com.shoppingcar.demo.service;

import com.shoppingcar.demo.models.dto.VentaDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service interface for the Sale entity.
 */
public interface IVentaService {

    VentaDTO save(VentaDTO clientDto);

    List<VentaDTO> findAll();

    Optional<VentaDTO> findOne(Long id);

    List<VentaDTO> findAllSalesById(Long id);
}
