package com.shoppingcar.demo.exceptions;

public class ClienteException extends Exception {
    public ClienteException(String message) {
        super(message);
    }
}
