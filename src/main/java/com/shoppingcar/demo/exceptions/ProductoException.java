package com.shoppingcar.demo.exceptions;

public class ProductoException extends Exception {
    public ProductoException(String message) {
        super(message);
    }
}
