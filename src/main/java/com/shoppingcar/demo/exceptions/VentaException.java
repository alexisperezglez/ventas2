package com.shoppingcar.demo.exceptions;

public class VentaException extends Exception {
    public VentaException(String message) {
        super(message);
    }
}
