package com.shoppingcar.demo.models.mapper;

import com.shoppingcar.demo.models.domain.Producto;
import com.shoppingcar.demo.models.dto.ProductoDTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity Product and its DTO ProductDto.
 */
@Mapper( componentModel = "spring")
@Component
public interface ProductoMapper extends EntityMapper<ProductoDTO, Producto> {

}
