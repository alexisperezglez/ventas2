package com.shoppingcar.demo.models.mapper;

import com.shoppingcar.demo.models.domain.Cliente;
import com.shoppingcar.demo.models.dto.ClienteDTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity Client and its DTO ClientDTO.
 */
@Mapper( componentModel = "spring")
@Component
public interface ClienteMapper extends EntityMapper<ClienteDTO, Cliente> {

}
