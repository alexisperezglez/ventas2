package com.shoppingcar.demo.models.mapper;

import com.shoppingcar.demo.models.domain.Venta;
import com.shoppingcar.demo.models.dto.VentaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity Sale and its DTO SaleDto.
 */
@Mapper( componentModel = "spring")
@Component
public interface VentaMapper extends EntityMapper<VentaDTO, Venta> {

    @Mapping(source = "cliente.id", target = "idCliente")
    @Mapping(source = "cliente.nombre", target = "nombreCliente")
    VentaDTO toDto(Venta venta);

    @Mapping(source = "idCliente", target = "client.id")
    Venta toEntity(VentaDTO ventaDTO);

    default Venta fromId(Long id) {
        if (id == null) {
            return null;
        }
        Venta venta = new Venta();
        venta.setId(id);
        return venta;
    }

}
