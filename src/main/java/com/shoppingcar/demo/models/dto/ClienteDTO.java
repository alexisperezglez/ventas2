package com.shoppingcar.demo.models.dto;

import lombok.*;

@Builder(builderMethodName = "newInstance") @AllArgsConstructor @NoArgsConstructor @Data
public class ClienteDTO {
    private Long id;
    private String nombre;
    private String apellido;
    private String dni;
    private String telefono;
    private String correo;
}
