package com.shoppingcar.demo.models.dto;

import lombok.*;

import java.time.ZonedDateTime;

@Data @NoArgsConstructor @AllArgsConstructor @ToString @Builder
public class VentaDTO {
    private Long id;
    private ZonedDateTime fecha;
    private Long idCliente;
    private String nombreCliente;

}
