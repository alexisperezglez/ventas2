package com.shoppingcar.demo.models.dto;

import lombok.*;

@Data @NoArgsConstructor @AllArgsConstructor @ToString @Builder
public class ProductoDTO {
    private Long id;
    private String nombre;
    private Float precio;
}
