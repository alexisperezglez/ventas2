package com.shoppingcar.demo.models.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Builder(builderMethodName = "newInstance") @AllArgsConstructor @NoArgsConstructor @Data
@Entity
@Table(name = "venta")
public class Venta {

    @Id
    @Column(name = "idVenta")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fecha", nullable = false)
    private ZonedDateTime fecha;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("ventas")
    private Cliente cliente;

    @OneToMany(mappedBy = "venta", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<VentaDetalle> ventaDetalle = new HashSet<>();

}
