package com.shoppingcar.demo.models.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data @NoArgsConstructor
@Entity
@Table(name = "detalleVenta")
public class VentaDetalle {

    @Id
    @Column(name = "idVentaDetalle")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("venta")
    private Venta venta;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("ventaDetalle")
    private Producto producto;
}
