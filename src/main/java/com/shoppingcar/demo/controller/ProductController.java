package com.shoppingcar.demo.controller;

import com.shoppingcar.demo.models.dto.ProductoDTO;
import com.shoppingcar.demo.exceptions.ProductoException;
import com.shoppingcar.demo.service.IProductoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api")
public class ProductController {

    private final IProductoService IProductoService;

    public ProductController(IProductoService IProductoService) {
        this.IProductoService = IProductoService;
    }

    public static void writeLog(String text) {
        log.error(text);
    }

    /**
     * POST  /products : Create a new product.
     *
     * @param productoDTO the productDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/products")
    public ResponseEntity<ProductoDTO> createProduct(@RequestBody ProductoDTO productoDTO) throws URISyntaxException, ProductoException {
        log.debug("REST request to save Product : {}", productoDTO);

        ProductoDTO result = IProductoService.save(productoDTO);
        return ResponseEntity.created(new URI("/api/products/" + result.getId()))
                .body(result);
    }

    /**
     * PUT  /products : Updates an existing product.
     *
     * @param productoDTO the productDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productDTO
     */
    @PutMapping("/products")
    public ResponseEntity<ProductoDTO> updateProduct(@RequestBody ProductoDTO productoDTO) throws ProductoException {
        log.debug("REST request to update Product : {}", productoDTO);
        ProductoDTO result = IProductoService.save(productoDTO);
        return ResponseEntity.ok()
                .body(result);
    }

    /**
     * GET  /products : get all the products.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of products in body
     */
    @GetMapping("/products")
    public List<ProductoDTO> getAllProducts() throws ProductoException {
        log.debug("REST request to get all Products");
        return IProductoService.findAll();
    }

    /**
     * GET  /products/:id : get the "id" product.
     *
     * @param id the id of the productDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productDTO
     */
    @GetMapping("/products/{id}")
    public ResponseEntity<ProductoDTO> getProduct(@PathVariable Long id) throws ProductoException {
        log.debug("REST request to get Product : {}", id);

        ProductoDTO productoDTO = IProductoService.findOne(id);
        return new ResponseEntity<>(productoDTO,HttpStatus.OK);
    }

    /**
     * DELETE  /products/:id : delete the "id" product.
     *
     * @param id the id of the productDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) throws ProductoException {
        log.debug("REST request to delete Product : {}", id);
        IProductoService.delete(id);
        return ResponseEntity.ok().build();
    }

}
