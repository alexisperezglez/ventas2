package com.shoppingcar.demo.controller;

import com.shoppingcar.demo.models.dto.ClienteDTO;
import com.shoppingcar.demo.service.IClienteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rx.Single;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/cliente")
@Slf4j
public class ClienteController {

	private final IClienteService IClienteService;

	public ClienteController(IClienteService IClienteService) {
		this.IClienteService = IClienteService;
	}

    public static void writeLog(String text) {

        log.error(text);

    }


    /**
     * POST  /cliente : Create a new client.
     *
     * @param clienteDTO the clientDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("")
    public ResponseEntity<ClienteDTO> createClient(@RequestBody ClienteDTO clienteDTO) throws URISyntaxException {
        log.debug("REST request to save Client : {}", clienteDTO);

        ClienteDTO result = IClienteService.save(clienteDTO);
        return ResponseEntity.created(new URI("/api/clients/" + result.getId()))
                .body(result);
    }

	/**
     * GET  /clients : get all the clients.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clients in body
     */
    @GetMapping("")
    public List<ClienteDTO> getAllClients() {
        log.debug("REST request to get all clients");
        return IClienteService.findAll();
    }


    @GetMapping(
            value = "{clientId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<Object> findAllById(@PathVariable("clientId") String id) {
        log.debug("REST request to get all clients");
        return IClienteService.findAllById(Long.parseLong(id));
    }

}
