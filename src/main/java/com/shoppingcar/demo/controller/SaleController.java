package com.shoppingcar.demo.controller;

import com.shoppingcar.demo.models.dto.VentaDTO;
import com.shoppingcar.demo.service.IVentaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
public class SaleController {

    private final IVentaService IVentaService;

    public SaleController(IVentaService IVentaService) {
		this.IVentaService = IVentaService;
	}

    public static void writeLog(String text) {
        log.error(text);
    }

    /**
     * POST  /sales : Create a new sale.
     *
     * @param ventaDTO the saleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new saleDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales")
    public ResponseEntity<VentaDTO> createSale(@RequestBody VentaDTO ventaDTO) throws URISyntaxException {
        log.debug("REST request to save Sale : {}", ventaDTO);

        VentaDTO result = IVentaService.save(ventaDTO);
        return ResponseEntity.created(new URI("/api/sales/" + result.getId()))
                .body(result);
    }

	/**
     * GET  /sales : get all the sales.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sales in body
     */
    @GetMapping("/sales")
    public List<VentaDTO> getAllSales() {
        log.debug("REST request to get all sales");
        return IVentaService.findAll();
    }


    @GetMapping("/sales/{id}")
    public List<VentaDTO> getAllSalesByUserId(@PathVariable Long id) {
        log.debug("Request to get all sales from userId : {}", id);
        return IVentaService.findAllSalesById(id);
    }

}
