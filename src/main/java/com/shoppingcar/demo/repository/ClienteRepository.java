package com.shoppingcar.demo.repository;

import com.shoppingcar.demo.models.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Client entity.
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

}
