package com.shoppingcar.demo.repository;

import com.shoppingcar.demo.models.domain.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Product entity.
 */
@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

}
