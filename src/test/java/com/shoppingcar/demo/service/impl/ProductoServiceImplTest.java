package com.shoppingcar.demo.service.impl;

import com.shoppingcar.demo.models.domain.Producto;
import com.shoppingcar.demo.models.dto.ProductoDTO;
import com.shoppingcar.demo.models.mapper.ProductoMapper;
import com.shoppingcar.demo.exceptions.ProductoException;
import com.shoppingcar.demo.repository.ProductoRepository;
import com.shoppingcar.demo.service.ProductoServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductoServiceImplTest implements TestConstants {

    @Rule
    public ExpectedException fails = ExpectedException.none();

    @Mock
    ProductoRepository productoRepository;
    @Mock
    ProductoMapper productoMapper;
    @InjectMocks
    private ProductoServiceImpl service;

    Producto producto = Producto.newInstance()
            .id(PRODUCT_ID)
            .nombre("bici")
            .precio(Float.valueOf("250.00"))
            .build();
    ProductoDTO productoDTO = ProductoDTO.builder()
            .id(PRODUCT_ID)
            .nombre("bici")
            .precio(Float.valueOf("250.00"))
            .build();

    @Before
    public void setUp() throws Exception {

        when(productoRepository.findById(PRODUCT_ID))
                .thenReturn(Optional.of(producto));
    }

    @Test(expected = NullPointerException.class)
    public void findOneByIdNotExist() throws ProductoException {
      Producto prod =  service.findById(PRODUCT_ID);
        assertEquals(PRODUCT_ID, prod.getId());
    }
}