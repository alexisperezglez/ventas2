package com.shoppingcar.demo.service.impl;

import com.shoppingcar.demo.models.domain.Venta;
import com.shoppingcar.demo.models.dto.VentaDTO;
import com.shoppingcar.demo.models.mapper.VentaMapper;
import com.shoppingcar.demo.repository.VentaRepository;
import com.shoppingcar.demo.service.VentaServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VentaServiceImplTest {

    @Rule
    public ExpectedException fails = ExpectedException.none();

    @Mock
    VentaRepository ventaRepository;
    @Spy
    VentaMapper ventaMapper;
    @InjectMocks
    private VentaServiceImpl service;

    Venta venta = Venta.newInstance().build();
    VentaDTO ventaDTO = VentaDTO.builder().build();

    @Before
    public void setUp() throws Exception {

        when(ventaRepository.save(any()))
                .thenReturn(null);
    }

    @Test
    public void saveNullEtityExpectException() {

        service.save(null);

        verify(ventaRepository, times(1))
                .save(null);


    }

}