package com.shoppingcar.demo.service.impl;

import com.shoppingcar.demo.models.domain.Cliente;
import com.shoppingcar.demo.models.dto.ClienteDTO;
import com.shoppingcar.demo.models.mapper.ClienteMapper;
import com.shoppingcar.demo.repository.ClienteRepository;
import com.shoppingcar.demo.service.ClienteServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.shoppingcar.demo.service.impl.TestConstants.CLIENT_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ClienteServiceImplTest {

    @Rule
    public ExpectedException fails = ExpectedException.none();

    @Mock
    ClienteRepository clienteRepository;
    @Mock
    ClienteMapper clienteMapper;
    @InjectMocks
    private ClienteServiceImpl service;

    Cliente cliente = Cliente.newInstance()
            .id(CLIENT_ID)
            .correo("correo@gmail.com")
            .nombre("correo")
            .apellido("de cuba")
            .dni("dni")
            .telefono("000-00000")
            .build();

    ClienteDTO clienteDTO = ClienteDTO.newInstance()
            .id(CLIENT_ID)
            .correo("correo@gmail.com")
            .nombre("correo")
            .apellido("de cuba")
            .dni("dni")
            .telefono("000-00000")
            .build();

    @Before
    public void setUp() throws Exception {

        when(clienteMapper.toEntity(clienteDTO)).thenReturn(cliente);
        when(clienteMapper.toDto(cliente)).thenReturn(clienteDTO);

        when(clienteRepository.save(cliente)).thenReturn(cliente);
        when(clienteRepository.findAll()).thenReturn(Arrays.asList(cliente));
    }

    @Test
    public void whenSaveProductAllParamsSet() {

        ClienteDTO clientdto = service.save(clienteDTO);

        verify(clienteRepository, times(1))
                .save(cliente);
        verify(clienteMapper, times(1))
                .toEntity(clienteDTO);
        verify(clienteMapper, times(1))
                .toDto(cliente);

        assertEquals(clientdto.getId(), CLIENT_ID);
    }

    @Test
    public void findAll() {

        List<ClienteDTO> clienteDTOList = service.findAll();

        verify(clienteRepository, times(1))
                .findAll();

        verify(clienteMapper, times(1))
                .toDto(cliente);

        assertTrue(!clienteDTOList.isEmpty());
        assertTrue(clienteDTOList.size() > 0);
    }

    @Test
    public void findAllById() {

    }
}